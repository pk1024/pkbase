# pkbuntu:

a ubuntu 16.04 image with my
custom toolkit and scripts.

This is for developing and managing my own tools.

## Release 1.0:
Included packages:
- sudo
- vim
- nano
- aptitude
- openssl 
- openssh-client
- openssh-server
- git
- curl
- wget
- netcat

## release-1.1:
Added some extra packages that will be required by zap and other images.
- unzip
- gcc g++
- autoconf
- automake

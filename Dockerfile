#-----------------------------------------
#	pkbase:Dockerfile
#	Author: Donald Raikes
#	Date: 08/29/2017
#-----------------------------------------
FROM	ubuntu:16.04
MAINTAINER	Donald Raikes <dr1861@nyu.edu>

##	Update the base operating system:
RUN		apt-get update && \
		apt-get upgrade -y && \
		apt-get install -y sudo \
			git \
			gzip \
			bzip2 \
			tar \
			curl \
			wget \
			vim \
			nano \
			aptitude \
			openssh-server \
			openssl \
			netcat \
			unzip \
			make \
			gcc g++ \
			automake \
			autoconf \
			ruby \
			ruby-dev && \
		apt-get clean 
RUN		echo "export PS1='\$PWD# '" >> /root/.bashrc && \
		echo "export TERM=vt320" >> /root/.bashrc
WORKDIR	/root
USER	root
